import authHeader from '@/_helpers/auth_header'
const apiHosts = apiHostname();

export default function getSelectedProductsByType(promoData) {
    const productsList = await fetch(apiHosts.api+'/api/promotion/'+promoData.promo_id+'/product?type='+promoData.product_type, {
        headers: authHeader()
    });
    const resp = await productsList.json();
    return resp;
}



