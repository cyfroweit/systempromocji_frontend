export default function apiHostname() {
    let apiHostname = {}
    apiHostname.api = 'http://promotion.firmaxl.pl';
    apiHostname.acl = 'http://acl.firmaxl.pl';
    return {acl: apiHostname.acl, api: apiHostname.api};
}