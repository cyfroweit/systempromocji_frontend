import router from '@/router'
import apiHostname from '@/_helpers/api_hostname'
import authHeader from '@/_helpers/auth_header'
const apiHosts = apiHostname();

export const actions = {
    
    async getPromotionList({state, commit, dispatch}, status_id) {
        dispatch('clearPromotionList');
        if (status_id) {
            const promoList = await fetch(apiHosts.api+'/api/promotion/?status='+status_id ,{
                headers: authHeader()
            });
            if (promoList.ok) {
                const resp = await promoList.json();
                commit('setPromotionList', resp.payload); 
            } else {
                if (promoList.status === 401) {
                    console.log('refresh token ->');
                    dispatch('user/refreshToken', '',{root:true});
                }
            }
        } else {
            const promoList = await fetch(apiHosts.api+'/api/promotion',{
                headers: authHeader()
            })
            const resp = await promoList.json();
                commit('setPromotionList', resp.payload);
        }
    },

    clearPromotionList(state) {
        state.commit('setPromotionList', false);
    },

    async clonePromotion(state, promotion_id) {
        const cloneReq = await fetch(apiHosts.api+'/api/promotion/'+promotion_id+'/clone',{
            method: 'PUT',
            cache: 'no-cache',
            headers: authHeader()
        });
        if (cloneReq.ok) {
            const resp = await cloneReq.json();
            state.commit("clearPromotionData");
            state.commit("clearPromotionMessages");
            router.push({ path: '/edycja_promocji/'+resp.payload.id });
            state.dispatch('setSuccessMessage', 'Klonowanie zakończone powodzeniem');
        } else {
            state.commit('setErrorMessage', 'Klonowanie zakończone niepowodzeniem. Błąd aplikacji. ('+cloneReq.status+')');
        }
    },

    async publishPromotion(state, promotion_id) {
        let rBody = {"status_id": 2};
        const publishReq = await fetch(apiHosts.api+'/api/promotion/'+promotion_id+'/status',{
            method: 'PUT',
            cache: 'no-cache',
            headers: authHeader(),
            body: JSON.stringify(rBody) 
        })

        if (publishReq.ok) {
            const resp = await publishReq.json();
            router.push({ path: '/lista_promocji/opublikowane' });
            state.dispatch('setSuccessMessage', 'Promocja została opublikowana');
        } else {
            state.commit('setErrorMessage', 'Publikacja zakończona niepowodzeniem. Błąd aplikacji. ('+publishReq.status+')');
        }
    },

    async stopPromotion(state, promotion_id) {
        let rBody = {"status_id": 3};
        const stopReq = await fetch(apiHosts.api+'/api/promotion/'+promotion_id+'/status',{
            method: 'PUT',
            cache: 'no-cache',
            headers: authHeader(),
            body: JSON.stringify(rBody) 
        })

        if (stopReq.ok) {
            const resp = await stopReq.json();
            router.push({ path: '/lista_promocji/archiwum' });
            state.dispatch('setSuccessMessage', 'Promocja została zakończona');
        } else {
            state.commit('setErrorMessage', 'Zatrzymanie promocji zakończone niepowodzeniem. Błąd aplikacji. ('+stopReq.status+')');
        }
    },
   
    async getInitialPromotionData(state, promoId) {
        if (promoId){
            const promo = await fetch(apiHosts.api+'/api/promotion/'+promoId,{
                headers: authHeader()
            });
            const resp = await promo.json();
            resp.payload.id = promoId;
            state.commit('setInitialPromoData', resp.payload);
        } else {
            state.commit('setInitialPromoData', null);
        }
    },
    async getPromotionTypes(state) {
        const promoTypes = await fetch(apiHosts.api+'/api/promotion/type', {
            headers: authHeader()
        });
        if (!promoTypes.ok) {
            state.commit('setErrorMessage', 'Wystąpił problem z serwerem.');
        }
        const resp = await promoTypes.json();
        state.commit('setPromotionTypes', resp.payload);
    },
    async addNewPromotion(state, promotionData) {
        let newPromodata = {
            type_id: promotionData.promotionType,
            name: promotionData.promotionName,
            tags: promotionData.promotionTags,
            start_date: new Date(promotionData.promotionDate.promotionStart),
            end_date: new Date(promotionData.promotionDate.promotionEnd),
            minBasketValue: promotionData.minBasketValue
        }

        const newPromotionReq = await fetch(apiHosts.api+'/api/promotion',{
            method: 'POST',
            cache: 'no-cache',
            headers: authHeader(),
            body: JSON.stringify(newPromodata) 
        });

        if (newPromotionReq.ok) {
           const resp = await newPromotionReq.json();
           let nextStepId;
           if (promotionData.promotionType === 1) {
                nextStepId = 11;
           } else if (promotionData.promotionType === 2){
                nextStepId = 21;
           }
           state.commit('changeStep', {step_id: nextStepId, promotion_id: resp.payload.id, promotion_type_id: resp.payload.type.id});
           router.push({ path: '/edycja_promocji/'+resp.payload.id });
           state.dispatch('setSuccessMessage', 'Dodano nową promocję');
        } else {
           state.commit('setErrorMessage', 'Błąd dodawania nowej promocji ('+newPromotionReq.status+')');
        }
    },
    async updatePromotion(state, promotionData) {
        let updatePromotionData = {
            name: promotionData.promotionName,
            tags: promotionData.promotionTags,
            start_date: new Date(promotionData.promotionDate.promotionStart),
            end_date: new Date(promotionData.promotionDate.promotionEnd),
            minBasketValue: promotionData.minBasketValue
        }
        
        let stepId = 11;
        if (promotionData.type_id === 2) stepId = 21;

        const updatePromotionReq = await fetch(apiHosts.api+'/api/promotion/'+promotionData.id,{
            method: 'PATCH',
            cache: 'no-cache',
            body: JSON.stringify(updatePromotionData),
            headers: authHeader()
        });

        if (updatePromotionReq.ok) {
            const resp = await updatePromotionReq.json();
            state.commit('changeStep', { step_id: stepId, promotion_id: resp.payload.id, promotion_type_id: resp.payload.type.id} );
        } else {
            state.commit('setErrorMessage', 'Błąd aktualizowania promocji ('+updatePromotionReq.status+')');
        }
    },
    clearProducts(state) {
        state.commit('clearProducts');
    },
    async fetchProductsByCategoryID(state, catId) {
        const productsList = await fetch(apiHosts.api+'/api/product/category/'+catId,{
            headers: authHeader()
        });
        const resp = await productsList.json();
        state.commit('setProductsByCategory', resp.payload);
    },
    async fetchSelectedProducts(state, promoData) {
        const productsList = await fetch(apiHosts.api+'/api/promotion/'+promoData.promo_id+'/product?type='+promoData.product_type, {
            headers: authHeader()
        });
        const resp = await productsList.json();
        state.commit('setSelectedProducts', resp.payload);
    },

    async fetchSelectedProductsByType(state, promoData) {
        
        const productsList1 = await fetch(apiHosts.api+'/api/promotion/'+promoData.promo_id+'/product?type=1', {
            headers: authHeader()
        });
        const productsList2 = await fetch(apiHosts.api+'/api/promotion/'+promoData.promo_id+'/product?type=2', {
            headers: authHeader()
        });
        const productsList3 = await fetch(apiHosts.api+'/api/promotion/'+promoData.promo_id+'/product?type=3', {
            headers: authHeader()
        });
        const resp1 = await productsList1.json();
        const resp2 = await productsList2.json();
        const resp3 = await productsList3.json();

        let products = {}
        products[1] = resp1.payload
        products[2] = resp2.payload
        products[3] = resp3.payload
        state.commit('setSelectedProductsByType', products );
    },

    addSelectedProducts(state, products) {
        const keys_to_keep = ['name', 'erpIndex', 'erpId'];

        const result = products.map(e => {
            const obj = {};
            keys_to_keep.forEach(k => obj[k] = e[k])
            obj.isActive = true;
            return obj;
        });
        state.commit('addSelectedProducts', result);
    },
    removeFromSelectedProducts(state, erpindex) {
        state.commit('removeFromSelectedProducts', erpindex);
    },
    changeQuantitySelectedProducts(state, changedProd) {
        state.commit('changeQuantitySelectedProducts', changedProd);
    },
    toggleQuantitySelectedProducts(state, erpIndex) {
        state.commit('toggleQuantitySelectedProducts', erpIndex);
    },
    async updateSelectedProducts({state, commit, getters}, promotionData) {
        const selectedProducts = getters.getSelectedProducts;
        function getItems(item) {
            return {erpId: item.erpId, isStockLimit: item.isStockLimit ? 1:0, quantityLimit: item.quantityLimit ? item.quantityLimit : 0 }
        }
        let selectedIds = selectedProducts.map(getItems);

        const selectedProductsReq = await fetch(apiHosts.api+'/api/promotion/'+promotionData.promotion_id+'/product/'+promotionData.product_type,{
            method: 'PATCH',
            cache: 'no-cache',
            headers: authHeader(),
            body: JSON.stringify(selectedIds) 
        });

        if (selectedProductsReq.ok) {
            commit('changeStep', {step_id: promotionData.step_id, promotion_id: promotionData.promotion_id} );
        } else {
            commit('setErrorMessage', 'Nastąpił błąd przy aktualizowaniu produktów ('+selectedProductsReq.status+')');
        }
    },
    async getCategories(state) {
        const categories = await fetch(apiHosts.api+'/api/product/category', {
            headers: authHeader()
        })
        const resp = await categories.json();
        state.commit('setCategories', resp.payload);
    },
    setSelectedCat(state, cat) {
        state.commit('setSelectedCat', cat);
    },
    async findByErpID(state, erpIndex) {
        const product = await fetch(apiHosts.api+'/api/product/?index='+erpIndex, {
            headers: authHeader()
        });
        const resp = await product.json();
        state.commit('setProductsByCategory', resp.payload);
        state.commit('setSelectedCat', null)
    },
    findByErpIDClear(state) {
        state.commit('findByErpIDClear');
    },
    clearPromotionData(state) {
        state.commit('clearPromotionData');
    },
    async promotionDecorators(state, promotionData) {
        const decoratorData = promotionData.formData;
        let rBody = {
            "lpUrl":decoratorData.lpURL, 
            "label": decoratorData.label, 
            "badge":decoratorData.badge,
            "basketMessage": promotionData.basketMessage
        }
        
        const decorationReq = await fetch(apiHosts.api+'/api/promotion/'+decoratorData.promotion_id+'/description',{
            method: 'PATCH',
            cache: 'no-cache',
            headers: authHeader(),
            body: JSON.stringify(rBody) 
        });
        if (decorationReq.ok) {
            const resp = await decorationReq.json();
            state.commit('changeStep', {step_id: promotionData.step_id, promotion_id: promotionData.promotion_id});
        } else {
            state.commit('setErrorMessage', 'Wystąpił błąd aplikacji ('+decorationReq.status+')');
        }
    },

    async fetchPromotionMessages(state, id) {
        const messagesReq = await fetch(apiHosts.api+'/api/promotion/'+id+'/message', {
            headers: authHeader()
        });

        if (messagesReq.ok) {
            const resp = await messagesReq.json();
              state.commit('setPromotionMessages', resp.payload);
        } else {
            state.commit('setErrorMessage', 'Nie można pobrać treści komunikatów ('+messagesReq.status+')');
        }
    },
    async updatePromotionMessages(state, promotionData) {
        let rBody = []
            for (let i = 0; i < promotionData.promotionMessages.length; i++) {
                rBody.push({
                    "message": promotionData.promotionMessages[i].message,
                    "promotionMessageType": promotionData.promotionMessages[i].promotionMessageType
                })
            }     
            
        const updateMessages = await fetch(apiHosts.api+'/api/promotion/'+promotionData.promotion_id+'/messages', {
            headers: authHeader(),
            method: 'POST',
            body: JSON.stringify(rBody) 
        });   

        if (updateMessages.ok) {
            state.commit('changeStep', {step_id: promotionData.step_id, promotion_id: promotionData.promotion_id} );
        } else {
            state.commit('setErrorMessage', 'Nastąpił błąd przy dodawaniu komunikatów ('+updateMessages.status+')');
        }
    },
    clearPromotionMessages(state) {
        state.commit('clearPromotionMessages');
    },
    changeStep(state, stepData) {
        state.commit('changeStep', stepData);
    },
    setErrorMessage(state, message) {
        state.commit('setErrorMessage', message);
    },
    setSuccessMessage(state, val) {
        state.commit('setSuccessMessage', val);
    }
}


