import Vue from 'vue'
import Vuex from 'vuex'
import { mutations } from './mutations'
import { getters } from './getters'
import { actions } from './actions'
import router from '@/router'

Vue.use(Vuex)

const state = {
  
  errorMessage: null,
  successMessage: null,
  initialPromoData: {},
  promotionData: {
    id: null,
    type_id: null,
    type_name: null,
    name: null,
    tags: '',
    start_date: null,
    end_date: null,
    user: 'default',
    setInitialPromoData: 0
  },
  promotionMessages: null,
  currentStep: {
    step_id: null,
    promotion_id: null,
    promotion_type_id: null
  },
  promotionList: [],
  sidebarVisible: true,
  promotionTypes: null,
  selectedProducts: [],
  selectedProductsByType: [],
  productsByCategory: [],
  categories: [],
  selectedCat: '',
  searchByErpId: false,
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
  router
}