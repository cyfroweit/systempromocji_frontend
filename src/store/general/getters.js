export const getters =  {
    getPromotionTypes: state => state.promotionTypes,
    getPromotionData: state => state.promotionData,
    getInitialPromotionData: state => state.initialPromoData,
    getCurrentStep: state => state.currentStep,
    getPromotionList: state => state.promotionList,
    getSelectedProducts: state => state.selectedProducts,
    getSelectedProductsByType: state => state.selectedProductsByType,
    getProductsByCategory: state => state.productsByCategory,
    getSearchByErpId: state => state.searchByErpId,
    getCategories: state => state.categories,
    getSelectedCat: state => state.selectedCat,
    getPromotionMessages: state => state.promotionMessages,
    getErrorMessage: state => state.errorMessage,
    getSuccessMessage: state => state.successMessage
  }