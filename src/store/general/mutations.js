export const mutations = {
    clearPromotionData(state) { //czyszczenie dotychczasowych danych pod nowa promocje
        
        state.promotionData = {
            id: null,
            type_id: null,
            name: '',
            start_date: null,
            end_date: null,
            user: 'default',
            description: {}
        }
        state.promotionMessages = [];

    },
    setPromotionTypes(state, payload) { // typy promocji wypelnienie state dla selecta
        state.promotionTypes = payload
    },
    selectPromotionType(state, e) { // wybor typu promocji z selecta 
        state.promotionData.type_id = e;
    },
    setPromotionName(state, e) {
        state.promotionData.name = e;
    },
    setPromotionTags(state, e) {
        state.promotionData.tags = e;
    },
    setPromotionStartDate(state ,e) {
        state.promotionData.start_date = e; 
    },
    setPromotionEndDate(state ,e) {
        state.promotionData.end_date = e; 
    },
    toggleSidebar(state) { 
        state.sidebarVisible++
    },
    setInitialPromoData(state, promoData) { //pobrane dane istniejacej promocji z api nadpisuja wstepnie dane formularza z zachowaniem oryginalnych
        if (promoData) {
            state.initialPromoData = promoData
            if (state.initialPromoData.description === null) {
                state.initialPromoData.description = {}
            }
        } 
        else {
            state.initialPromoData = {}
        }
     },
    setPromotionList(state, promotionList) {
        promotionList ? state.promotionList = promotionList : state.promotionList = []
    },
    clearProducts(state) {
        state.productsByCategory = []; 
    },
    setProductsByCategory(state, products) {
        state.productsByCategory = products; 
    },
    setSelectedProducts(state, selectedProducts) {
        function extractProductData(item) {
            return {
                erpId: item.product.erpId,
                erpIndex: item.product.erpIndex,
                name: item.product.name,
                isActive: item.product.isActive,
                isDeleted: item.product.isDeleted,
                quantityLimit: item.promotionProductProperty ? item.promotionProductProperty.quantityLimit : null,
                isStockLimit: item.promotionProductProperty ? item.promotionProductProperty.isStockLimit  : null
            }
        }
        let productInfo = selectedProducts.map(extractProductData);
        state.selectedProducts = productInfo;
    },

    setSelectedProductsByType(state, products) {
        state.selectedProductsByType = products;
    },

    addSelectedProducts(state,products) {
        const mergedProducts = state.selectedProducts.concat(products)
        const uniqByProp = prop => arr =>
            Object.values(
                arr.reduce((acc, item) => (item && item[prop] && (acc[item[prop]] = item), acc), {})
            );
        const uniqueById = uniqByProp("erpId");
        const unifiedArray = uniqueById(mergedProducts);
        state.selectedProducts = unifiedArray

    },
    removeFromSelectedProducts(state, erpindex) {
        state.selectedProducts = state.selectedProducts.filter(function( obj ) {
            return obj.erpIndex!== erpindex;
        });
    },
    changeQuantitySelectedProducts(state, changedProd) {
        const elIndex = state.selectedProducts.findIndex(e => e.erpIndex == changedProd.erpIndex);
        if( elIndex>=0) {
            state.selectedProducts[elIndex].quantityLimit = changedProd.quantityLimit;
        }      
    },
    toggleQuantitySelectedProducts(state, erpIndex) {
        const elIndex = state.selectedProducts.findIndex(e => e.erpIndex == erpIndex);
        if( elIndex>=0) {
            state.selectedProducts[elIndex].isStockLimit = !state.selectedProducts[elIndex].isStockLimit
        }      
    },
    setCategories (state, categories) {
        state.categories = categories;
    },
    setSelectedCat(state, cat) {
        state.selectedCat = cat;
    },
    findByErpID (state, result) {
        state.searchByErpId = result;
    },
    findByErpIDClear(state) {
        state.searchByErpId = false
    },
    setPromotionMessages(state, messages) {
        let extractedMessages = {}
        let returnMessageByType = function(msg) {
            switch (msg.promotionMessageType.id) {
                case 1:
                    extractedMessages['positive'] = msg.message
                    break;
                case 2:
                    extractedMessages['negative'] = msg.message
                    break;
                case 3:
                    extractedMessages['order'] = msg.message
                    break;
                default:
                    console.log('brak typu komunikatu');
            }
        }
        messages.map(returnMessageByType);      
        state.promotionMessages = extractedMessages;
    },
    clearPromotionMessages(state) {
        state.promotionMessages = null;
    },
    changeStep(state, stepData) {
         if (stepData.promotion_id === null) {
            stepData.promotion_type_id = null
            stepData.step_id = null
        }
        state.currentStep = { 
            step_id: stepData.step_id, 
            promotion_id: stepData.promotion_id,
            promotion_type_id: stepData.promotion_type_id
        }
    },
    setErrorMessage(state, message) {
        state.errorMessage = message;
    },
    setSuccessMessage(state, val) {
        state.successMessage = val;
    }
}