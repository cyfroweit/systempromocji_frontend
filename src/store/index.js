import Vue from 'vue'
import Vuex from "vuex";
import general from "./general";
import user from "./user";

Vue.use(Vuex)

export default new Vuex.Store({
    strict: true,
    modules: {
        user,
        general,   
    },
  })