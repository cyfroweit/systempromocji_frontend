export const getters =  {
    getUserData: (state, rootGetters) => state.user_data,
    getAuthData: (state, rootGetters) => state.auth_data
  }