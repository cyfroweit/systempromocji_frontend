import router from '@/router'
import apiHostname from '@/_helpers/api_hostname'
import authHeader from '@/_helpers/auth_header'
const apiHosts = apiHostname();

export const actions =  {

    async loginUser(state, credential) {
        let formParams = {
            "grant_type": "password",
            "client_id": "system_promocji_cyfrowe",
            "client_secret": "TyYWTyfbgRR61TMG3RnkOMXrqJ6eftfXgT",
            "scope": "*",
            "username": credential.email,
            "password": credential.password
        }

        const rBody = Object.keys(formParams).map((key) => {
            return encodeURIComponent(key) + '=' + encodeURIComponent(formParams[key]);
        }).join('&');
    
        const loginReq = await fetch(apiHosts.acl+'/api/oauth/access_token',{
            method: 'POST',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: rBody
        })

        if (loginReq.ok) {
            const resp = await loginReq.json();
            state.commit('setLoggedUser', resp);
            state.dispatch('general/setSuccessMessage', 'Zalogowano', {root:true});
            localStorage.setItem('user', JSON.stringify(resp));
            router.push({ path: '/' });
        } else {
            const resp = await loginReq.json();
            state.commit('general/setErrorMessage', 'Logowanie nie powiodło się. Błąd ('+loginReq.status+')', {root:true});
        }
    },

    async initialUser(state) {
        const localUser = JSON.parse(localStorage.getItem('user'));
        if(localUser && localUser.access_token ) {
            const validateReq = await fetch(apiHosts.acl+'/api/oauth/validate',{
                headers: authHeader(),
            });

            if ( validateReq.ok) {
                state.commit('setLoggedUser', localUser)
            } else {
                state.dispatch('refreshToken');
            }
          } else {
            state.commit('logout');
        }
    },

    async refreshToken({state, commit, dispatch, getters}) {
        const auth_data = getters.getAuthData;
        const formParams = {
            "grant_type": "refresh_token",
            "client_id": "system_promocji_cyfrowe",
            "client_secret": "TyYWTyfbgRR61TMG3RnkOMXrqJ6eftfXgT",
            "refresh_token": auth_data.refresh_token
        }

        const rBody = Object.keys(formParams).map((key) => {
            return encodeURIComponent(key) + '=' + encodeURIComponent(formParams[key]);
        }).join('&');

        const refreshReq = await fetch(apiHosts.acl+'/api/oauth/refresh_access_token',{
            method: 'POST',
            cache: 'no-cache',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: rBody
        })

        if (refreshReq.ok) {
            const resp = await refreshReq.json();
            localStorage.setItem('user', JSON.stringify(resp));
        } else {
            //commit('general/setErrorMessage', 'Nastąpiło wylogowanie ('+refreshReq.status+')', {root:true});
            dispatch('logout');
        }
    },

    logout(state) {
        state.commit('logout');
        localStorage.removeItem('user');
        router.push({ path: '/login'});
    }

}