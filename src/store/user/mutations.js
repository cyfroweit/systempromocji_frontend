import moment from 'moment';

export const mutations = {
    setLoggedUser(state, userInfo) {
        state.user_data.logged = true
        state.user_data.user_name = userInfo.user_name
        state.user_data.user_role = userInfo.user_role
        state.auth_data.expires_in = userInfo.expires_in
        state.auth_data.access_token = userInfo.access_token
        state.auth_data.refresh_token = userInfo.refresh_token
        state.auth_data.expire_time = moment().add(userInfo.expires_in, 'seconds').format();
    },
    logout(state) {
        state.user_data.logged = false
        state.user_data.user_name = null
        state.user_data.user_role = null
        state.auth_data.expires_in = null
        state.auth_data.access_token = null
        state.auth_data.refresh_token = null
        state.auth_data.expire_time = null
    }
}