import Vue from 'vue'
import Vuex from 'vuex'
import { mutations } from './mutations'
import { getters } from './getters'
import { actions } from './actions'
// import router from '@/router'

Vue.use(Vuex)

const state = {
    user_data: {
        logged: false,
        user_name: null,
        user_role: null,
    },
    auth_data: {
        grant_type: 'refresh_token',
        client_id: 'system_promocji',
        client_secret: 'secret',
        access_token: null,
        refresh_token: null,
        expires_in: null,
        expire_time: null
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }