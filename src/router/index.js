import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import NewPromotion from '../views/DefineNewPromo.vue'
import PromotionList from '../views/PromotionList.vue'
import ProductsList from '../views/ProductsList.vue'
import EditPromotion from '../views/EditPromotion.vue'
import PreviewPromotion from '../views/PreviewPromotion.vue'
import LoginPage from '../views/LoginPage'

import store from '../store/index.js'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: "Panel promocji"
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: LoginPage
  },
  {
    path: '/nowa_promocja',
    name: 'New',
    component: NewPromotion,
    meta: {
      title: "Panel promocji - Nowa promocja"
    }
  },
  {
    path: '/lista_produktow',
    name: 'ProductsList',
    component: ProductsList,
    meta: {
      title: "Panel promocji - Lista produktów"
    }
  },
  {
    path: '/edycja_promocji/:id',
    name: 'EditPromotion',
    component: EditPromotion,
    meta: {
      title: "Panel promocji - Edycja promocji"
    }
  },
  {
    path: '/podglad_promocji/:id',
    name: 'PreviewPromotion',
    component: PreviewPromotion,
    meta: {
      title: "Panel promocji - Podgląd promocji"
    }
  },
  {
    path: '/lista_promocji/:id',
    name: 'PromotionList',
    component: PromotionList,
    children: [
      {
      path: '',
      component: PromotionList,
      meta: {
        title: "Panel promocji - Lista promocji"
      }
      },
      {
        path: 'archiwum',
        component: PromotionList
      },
      {
        path: 'opublikowane',
        component: PromotionList
      },
      {
        path: 'szkice',
        component: PromotionList
      },
      {
        path: 'wszystkie',
        component: PromotionList
      }
    ]
  },
  {
    path: '*',
    redirect: '/'
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  store
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/login'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('user');

  const stateLogged = store.getters['user/getAuthData'].access_token;

  if (authRequired && (!stateLogged)) {
    if(loggedIn) {
      next()
    } else {
      return next('/login');
    }
  } else {
    next();
  }
})

export default router
